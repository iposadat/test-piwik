FROM php:7.1-apache

MAINTAINER "Ismael Posada Trobo <ismael.posada.trobo@cern.ch>"

# install the PHP extensions we need
RUN set -ex; \
	\
	apt-get update; \
	apt-get install -y \
		libjpeg-dev \
		libpng12-dev \
	; \
	rm -rf /var/lib/apt/lists/*; \
	\
	docker-php-ext-configure gd --with-png-dir=/usr --with-jpeg-dir=/usr; \
	docker-php-ext-install gd mysqli opcache
# TODO consider removing the *-dev deps and only keeping the necessary lib* packages

RUN a2enmod rewrite expires

# set recommended PHP.ini settings
# see https://secure.php.net/manual/en/opcache.installation.php

COPY php.ini /usr/local/etc/php/php.ini

#ENV PIWIK_VERSION 3.0.1

#RUN rm -rf /var && mkdir -p /var
#RUN curl -L -o /var/www/html/piwik.tar.gz "https://builds.piwik.org/piwik-${PIWIK_VERSION}.tar.gz"
#RUN tar -C /var/www/html/ -xf /var/www/html/piwik.tar.gz
#RUN rm /var/www/html/piwik.tar.gz

# WORKDIR is /var/www/html (inherited via "FROM php")
# "/entrypoint.sh" will populate it at container startup from /usr/src/piwik
VOLUME /var/www/html

#COPY docker-entrypoint.sh /entrypoint.sh

#ENTRYPOINT ["/entrypoint.sh"]

CMD ["apache2-foreground"]
